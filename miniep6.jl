# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    if (n < 1)
        return "Valor inválido! Chame a função com um valor maior ou igual a 1."
    else
    # Criaremos um vetor auxiliar com os números ímpares pré-computados, de onde tiraremos nossos ímpares
        lista = fill(0,999)
        j = 1
        for i=1:999
            lista[i] = j
            i += 1
            j += 2 
        end
        # Para descobrir o índice inicial no vetor lista para obter os ímpares consecutivos de n, usaremos outro loop for. 
        indice_inicial = 1
        for k=1:(n-1)
            indice_inicial = indice_inicial + (n-1)
            n -= 1
        end
        return lista[indice_inicial]
    end
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    if (m < 1)
        return "Valor inválido! Chame a função com um valor maior ou igual a 1."
    else
    # Imprimiremos m e criaremos um vetor auxiliar com os números ímpares pré-computados, de onde tiraremos nossos ímpares
        print(m)
        print(" ")
        print(m^3)
        print(" ")
        lista = fill(0,999)
        j = 1
        for i=1:999
            lista[i] = j
            i += 1
            j += 2 
        end
        # Para descobrir os índices a serem impressos, usaremos outros loops for 
        indice_inicial = 1
        o = m # a variável o recebrá o valor de m, pra não nos dar problemas com m na hora de imprimir
        for k=1:(o-1)
            indice_inicial = indice_inicial + (o-1)
            o -= 1
        end
        l = 0
        while l < m
            l = l+1
            print(lista[indice_inicial])
            print(" ")
            indice_inicial += 1
        end
    end 
end
        
        
function mostra_n(n)
    for i=1:n
        imprime_impares_consecutivos(i)
        println(" ")
        i += 1
    end 
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
    println("Tudo ok!") # Teste automatizado modificado nesta linha! 
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
